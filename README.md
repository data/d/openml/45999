# OpenML dataset: M3-competition-other

https://www.openml.org/d/45999

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

M3-Competition for time series forecasting, other data

From original paper:
-----
The 3003 series of the M3-Competition were selected on a quota basis to include various types of time series data (micro, industry, macro, etc.) 
and different time intervals between successive observations (yearly, quarterly, etc.). In order to ensure that enough data were available to 
develop an adequate forecasting model it was decided to have a minimum number of observations for each type of data.
This minimum was set as 14 observations for yearly series (the median length for the 645 yearly series is 19 observations), 
16 for quarterly (the median length for the 756 quarterly series is 44 observations), 48 for monthly (the median length for the 1428 monthly
series is 115 observations) and 60 for 'other' series (the median length for the 174 'other' series is 63 observations). Table 1 shows the
classification of the 3003 series according to the two major groupings described above. All the time series data are strictly positive; a test
has been done on all the forecasted values: in the case of a negative value, it was substituted by zero. This avoids any problem in the various 
MAPE measures.

As in the M-Competition, the participating experts were asked to make the following numbers of forecasts beyond the available data they had been 
given: six for yearly, eight for quarterly, 18 for monthly and eight for the category 'other'. Their forecasts were, subsequently, compared by 
the authors (the actual values referred to such forecasts were not available to the participating experts when they were making their forecasts 
and were not, therefore, used in developing their forecasting model). A presentation of the accuracy of such forecasts together with a discussion
of the major findings is provided in the next section.
-----

The time series were melted and restructured in 4 columns:

id_series: The identifier of a time series.

Category: The category of a time series.

Value: The value of the time series at 'time_step'.

time_step: The time step on the time series.

Preprocessing:

1 - Melted the data, obtaining columns 'time_step' and 'Value'.

2 - Dropped nan values.

The nan values correspond to time series that are shorter than the time series with maximum lenght, there are no nans in the middle of a time series.

3 - Dropped columns 'N', 'NF' and renamed column 'Series' to 'id_series'.

This values can be recreated in preprocessing steps if needed. N was the total number of observations. NF was the required number of forecast values
to be forecasted for each time series, for the 'other' dataset it was always 8. Therefore, if one wants to evaluate their model to be compared with
other models from the original competition, the last 8 values of each time series are considered the test dataset.

4 - Defined columns 'id_series' and 'Category' as 'category' and casted 'time_step' to int.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45999) of an [OpenML dataset](https://www.openml.org/d/45999). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45999/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45999/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45999/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

